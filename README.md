<b>Geolocation django project</b><br>
Django application, no usage of ORM

Uses Google API to fetch requested data
Implements 3 endpoints, pls refer to docstrings in views and utisl for more details

How to put your your own google API_KEY: change serve.util.API_KEY

<b>Endpoints:</b>

Return geocode by human-readable name of place

    views.get_geocode
 
    http://localhost:8000/serve/get_geocode?name=1600+Amphitheatre+Parkway,+Mountain+View,+CA

    {
        "lat": 37.4218651,
        "lon": -122.0846744
    }

Return list of places that can be found at particular coordinates

    views.reverse_geocode

    http://localhost:8000/serve/reverse_geocode?lat=40.714224;lon=-73.961452
    
    {
        "places": [
            "277 Bedford Ave, Brooklyn, NY 11211, USA",
            "279 Bedford Ave, Brooklyn, NY 11211, USA",
            "279 Bedford Ave, Brooklyn, NY 11211, USA",
            "279 Bedford Ave, Brooklyn, NY 11211, USA",
            "291-275 Bedford Ave, Brooklyn, NY 11211, USA",
            "South Williamsburg, Brooklyn, NY, USA",
            "Brooklyn, NY 11211, USA",
            "Brooklyn, NY, USA",
            "Kings County, Brooklyn, NY, USA",
            "New York, NY, USA",
            "Long Island, New York, USA",
            "New York, USA",
            "United States"
        ]
    }

Return distance in km between two places, passed in human-readable format

    views.get_path_length

    http://localhost:8000/serve/get_path_length?from_place=1600+Amphitheatre+Parkway,+Mountain+View,+CA;to_place=1000+Amphitheatre+Parkway,+Mountain+View,+CA

    {
        "distance": 0.48509572772448317
    }

<b>Running tests</b><br>

    python manage.py test
