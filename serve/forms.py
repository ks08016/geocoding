from django import forms

class UploadRouteForm(forms.Form):
    file = forms.FileField()