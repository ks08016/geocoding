from urllib import request
import json
import math
import re

API_KEY = '___________'
EARTH_RADIUS = 6371  # in km

def google_get_geocode(place: str):
    """
    Get geocode from google by place name
    :param place: place name
    :return: tuple: (latitude, longitude)
    """
    place = place.replace(' ', '+')
    place = re.sub("[^a-zA-Z0-9\+]", "", place)
    with request.urlopen('https://maps.googleapis.com/maps/api/geocode/json?address={}&key={}'.
                                        format(place, API_KEY)) as response:
        res = json.loads(response.read().decode('utf-8'))
        location = res['results'][0]['geometry']['location']
        return location['lat'], location['lng']


def google_reverse_geocode(lat, lon):
    """
    Return name of place by longitude and latitude
    :param lat: latitude
    :param lon: longitude
    :return:
    """
    with request.urlopen('https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&key={}'.
                                        format(lat, lon, API_KEY)) as response:
        res = json.loads(response.read().decode('utf-8'))
        return [a['formatted_address'] for a in res['results']]

def calculate_distance(lat1: float, lon1: float, lat2: float, lon2: float):
    """
    Calcuate geometric distance between two dots: lat1,lon1 and lat2, lon2
    :return: distance in km
    """

    def degreesToRadians(degrees):
        return degrees * math.pi / 180

    dLat = degreesToRadians(lat2 - lat1)
    dLon = degreesToRadians(lon2 - lon1)

    lat1 = degreesToRadians(lat1)
    lat2 = degreesToRadians(lat2)


    a = math.sin(dLat / 2) * math.sin(dLat / 2) + math.sin(dLon / 2) * \
        math.sin(dLon / 2) * math.cos(lat1) * math.cos(lat2)

    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    return EARTH_RADIUS * c
