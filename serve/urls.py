from django.conf.urls import url

from . import views

app_name = 'serve'
urlpatterns = [
    url('get_geocode', views.get_geocode),
    url('reverse_geocode', views.reverse_geocode),
    url('get_path_length', views.get_path_length),
]