import logging

from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseServerError
from django.http import JsonResponse

from .util import google_get_geocode, google_reverse_geocode, calculate_distance


def get_geocode(request):
    """
    Get geocode from google by place name
    """
    try:
        name = request.GET['name']
    except Exception as e:
        logging.error("Could not fetch places parameters", e)
        return HttpResponseBadRequest("Could not fetch places parameters")
    try:
        res = google_get_geocode(name)
        return JsonResponse({'lat': res[0], 'lon': res[1]})
    except Exception as e:
        logging.error("Could not fetch geocode", e)
        return HttpResponseServerError("Could not fetch geocode")


def reverse_geocode(request):
    """
    Return name of place by longitude and latitude
    """
    try:
        lon, lat = request.GET['lon'], request.GET['lat']
    except Exception as e:
        logging.error("Could not fetch parameters", e)
        return HttpResponseBadRequest("Could not fetch parameters")
    try:
        return JsonResponse({'places': google_reverse_geocode(lat, lon)})
    except Exception as e:
        logging.error("Could not reverse geocode", e)
        return HttpResponseServerError("Could not reverse geocode")


def get_path_length(request):
    """
    Get path by coordinates
    """
    try:
        from_place, to_place = request.GET['from_place'], request.GET['to_place']
    except Exception as e:
        logging.error("Could not fetch places parameters", e)
        return HttpResponseBadRequest("Could not fetch places parameters")
    try:
        return JsonResponse({'distance': calculate_distance(*google_get_geocode(from_place),
                                                            *google_get_geocode(to_place))})
    except Exception as e:
        logging.error("Could not calculate distance", e)
        return HttpResponseServerError("Could not reverse geocode")


