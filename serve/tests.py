from unittest import mock
from serve import util
from serve.util import API_KEY

from django.test import TestCase


class TestGeo(TestCase):

    def test_google_get_geocode(self):
        with mock.patch("serve.util.request") as req:
            m = mock.MagicMock(return_value=mock.MagicMock())
            req.urlopen = m
            try:
                util.google_get_geocode('Hello')
            except Exception as e:
                pass
            self.assertEquals(m.call_count, 1)
            self.assertEquals(m.call_args[0][0],
                              'https://maps.googleapis.com/maps/api/geocode/json?address=Hello&key={}'.format(API_KEY))

    def test_google_reverse_geocode(self):
        with mock.patch("serve.util.request") as req:
            m = mock.MagicMock(return_value=mock.MagicMock())
            req.urlopen = m
            try:
                util.google_reverse_geocode('123', '234')
            except Exception as e:
                pass
            self.assertEquals(m.call_count, 1)
            self.assertEquals(m.call_args[0][0],
                              'https://maps.googleapis.com/maps/api/geocode/json?latlng=123,234&key={}'.format(API_KEY))

    def test_calculate_distance(self):
        self.assertEquals(int(util.calculate_distance(0, 0, 123, 234)), 7931)
        self.assertEquals(int(util.calculate_distance(0, 0, 0, 0)), 0)
